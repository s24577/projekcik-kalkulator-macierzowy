# Projekt - kalkulator macierzowy

Należy przygotować program, który umożliwi użytkownikowi wykonywanie operacji na macierzach:

dodawanie i odejmowanie macierzy, 

mnożenie macierzy, 

transpozycja macierzy, 

obliczanie wyznacznika macierzy stopnia 2. 

Program działa w następujący sposób: 

Użytkownik podaje liczbę między 1 a 4, odpowiadającą operacji, jaką chce wykonać 

Użytkownik podaje kolejno ilość wierszy i ilość kolumn w macierzy 

Użytkownik podaje kolejne elementy macierzy, najpierw w pierwszym wierszu, potem w kolejnym itd. 

Jeżeli wybrana operacja była działaniem na dwóch macierzach, wówczas dodatkowo: 

Użytkownik podaje kolejno ilość wierszy i ilość kolumn w drugiej macierzy 

Użytkownik podaje kolejne elementy drugiej macierzy, najpierw w pierwszym wierszu, potem w kolejnym itd. 

Program podaje kolejno ilość wierszy i ilość kolumn w macierzy będącej wynikiem operacji 

Program podaje kolejne elementy w macierzy wynikowej, najpierw w pierwszym wierszu, potem w kolejnym itd. - elementy powinny być rozdzielone spacjami 

Należy przyjąć, że elementy w macierzy wejściowej (macierzach wejściowych) powinny być liczbami całkowitymi, zaś elementy macierzy wynikowej powinny być wartościami zmiennoprzecinkowymi. 

Jeżeli wybrana operacja nie może zostać wykonana na wprowadzonej macierzy (wprowadzonych macierzach), wówczas program powinien wypisać na konsolę komunikat “ERR” i prawidłowo się zakończyć.
