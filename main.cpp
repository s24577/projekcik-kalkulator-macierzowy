#include <iostream>
#include<conio.h>
#include <windows.h>
#include<math.h>

using namespace std;

//Funkcja obliczajaca wyznacznik macierzy - funkcja zwraca wyznacznik macierzy
int determinant( int matrix[10][10], int q) { //wymiary macierzy podawane sa w funkcji main
   int det = 0;
   int submatrix[10][10];
   if (q == 2) // jezeli rozmiar macierzy wynosi 2 to wyznacznik jest od razu obliczany
      return ((matrix[0][0] * matrix[1][1]) - (matrix[1][0] * matrix[0][1]));
   else { //jezeli rozmiar macierzy nie wynosi 2 to wyznacznik obliczany jest rekurencyjnie (funkcja wywołuje sama siebie)
      for (int x = 0; x < q; x++) { //trzy petle sluzace do obliczania wyznacznika
            int subi = 0;
            for (int i = 1; i < q; i++) {
               int subj = 0;
               for (int j = 0; j < q; j++) {
                  if (j == x)
                  continue;
                  submatrix[subi][subj] = matrix[i][j];
                  subj++;
               }
               subi++;
            }
            det = det + (pow(-1, x) * matrix[0][x] * determinant( submatrix, q - 1 )); // pow - podnoszenie liczby do potegi
      } //funkcja determinant() jest wywolywana reurencyjnie w celu oblcizenia wewnetrznego wyznacznika a nastepnie pomnozenia go przez wartosc zewnetrzna
   }
   return det;
}

int main()
{
    system("cls"); // czysci ekran konsoli
    int a[50][50],b[50][50],c[50][50],trans[50][50], matrix[10][10], i,j,m,n,o,p,l=1,r;
    cout <<"==============================================="<<endl;
    cout <<"==============KALKULATOR MACIERZY=============="<<endl;
    cout <<"================Broszko Grzegorz==============="<<endl;
    cout <<"====================s24577====================="<<endl;
    cout <<"==============================================="<<endl;
    cout <<"                  MENU GLOWNE                "<<endl;
    cout <<"==============================================="<<endl;
    cout <<"1. Dodawanie macierzy"<<endl;
    cout <<"2. Odejmowanie macierzy"<<endl;
    cout <<"3. Mnozenie macierzy"<<endl;
    cout <<"4. Transpozycja macierzy"<<endl;
    cout <<"5. Obliczanie wyznacznika macierzy"<<endl;
    cout <<"==============================================="<<endl;
    cout<<endl;

  do //Pętla do while pozwalajaca na powtarzanie programu
    {
    cout<<endl;
    cout<<"Wybierz dzialanie ktore chcesz wykonac (od 1 do 5): ";
    cin>>r;


      switch(r) //warunek wielokrotnego wyboru - pozwala podejmowac decyzje
    {
        //DODAWANIE MACIERZY
        case 1:
            cout<<"Podaj liczbe wierszy macierzy A: ";
            cin>>m;
            cout<<"Podaj liczbe kolumn macierzy A: ";
            cin>>n;
            cout<<"Podaj liczbe wierszy macierzy B: ";
            cin>>o;
            cout<<"Podaj liczbe kolumn macierzy B: ";
            cin>>p;

            if (m==o&&n==p)
            {
                cout<<endl;
                cout<< "Mozna dodac macierze."<<endl;
                cout<<endl;
            }
            else
            {
                cout<<endl;
                cout<<"Dodawanie macierzy nie jest mozliwe, sprobuj ponownie.";
                cout<<endl;
                cout<<"Aby wykonac kolejne dzialanie wcisnij Enter" << endl;
                break; // przerywa wyokonywanie danej instrukcji
                l=0;
            }

            if(l)
            {
                cout<<"Wprowadz wartosci macierzy A i potwierdzaj przyciskiem Enter: "<<endl;
                //Przechowywanie elemetnów pierwszej macierzy wprowadzonych przez u¿ytkownika
                for (i=0;i<m;++i)
                    for (j=0;j<n;++j)
                    {
                        cout << "Podaj element a" << i + 1 << j + 1 << " : ";
                        cin>>a[i][j];
                    }

                cout<<endl;
                cout<<"Macierz A: "<< endl;
                for (i=0;i<m;++i)
                {
                    cout<<endl;
                    for (j=0;j<n;++j)
                        cout<<a[i][j]<<" ";
                }
                cout<<endl;
                cout<<endl;
                cout<<"Wprowadz wartosci macierzy B i potwierdzaj przyciskiem Enter: "<<endl;
                //Przechowywanie elemetnów drugiej macierzy wprowadzonych przez u¿ytkownika
                for (i=0;i<o;++i)
                 for (j=0;j<p;++j)
                    {
                        cout << "Podaj element b" << i + 1 << j + 1 << " : ";
                        cin>>b[i][j];
                    }

                cout<<endl;
                cout<<"Macierz B: " << endl;
                for (i=0;i<o;++i)
                {
                    cout<<endl;
                    for (j=0;j<p;++j)
                        cout<<b[i][j]<<" ";
                }

                //Dodawanie dwoch macierzy
                cout<<endl;
                cout<<endl;
                cout<<"Wynikiem dodawania jest macierz: ";
                cout<<endl;
                for (i=0;i<m;++i)
                {
                    for (j=0;j<n;++j)
                    {
                        c[i][j]=a[i][j]+b[i][j];
                    }
                }
                for (i=0;i<m;++i)
                {
                    cout<<endl;
                    for (j=0;j<n;++j)
                        cout<<c[i][j]<<" ";
                }
            }
            cout<<endl;
            cout<<"Aby wykonac kolejne dzialanie wcisnij Enter" << endl;
            break;

        //ODEJMOWANIE MACIERZY
        case 2:
            cout<<"Podaj liczbe wierszy macierzy A: ";
            cin>>m;
            cout<<"Podaj liczbe kolumn macierzy A: ";
            cin>>n;
            cout<<"Podaj liczbe wierszy macierzy B: ";
            cin>>o;
            cout<<"Podaj liczbe kolumn macierzy B: ";
            cin>>p;

            if (m==o&&n==p)
            {
                cout<<endl;
                cout<<"Mozna odjac macierze "<<endl;
                cout<<endl;

            }
            else
            {
                cout<<endl;
                cout<<"Odejmowanie macierzy nie jest mozliwe, sprobuj ponownie.";
                cout<<endl;
                cout<<"Aby wykonac kolejne dzialanie wcisnij Enter" << endl;
                break;
                l=0;
            }

            if(l)
           {
                cout<<"Wprowadz wartosci macierzy A i potwierdzaj przyciskiem Enter: "<<endl;
                //Przechowywanie elemetnów pierwszej macierzy wprowadzonych przez u¿ytkownika
                for (i=0;i<m;++i)
                    for (j=0;j<n;++j)
                    {
                        cout << "Podaj element a" << i + 1 << j + 1 << " : ";
                        cin>>a[i][j];
                    }

                cout<<endl;
                cout<<"Macierz A: "<< endl;
                for (i=0;i<m;++i)
                {
                    cout<<endl;
                    for (j=0;j<n;++j)
                        cout<<a[i][j]<<" ";
                }
                cout<<endl;
                cout<<endl;
                cout<<"Wprowadz wartosci macierzy B i potwierdzaj przyciskiem Enter: "<<endl;
                //Przechowywanie elemetnów drugiej macierzy wprowadzonych przez u¿ytkownika
                for (i=0;i<o;++i)
                 for (j=0;j<p;++j)
                    {
                        cout << "Podaj element b" << i + 1 << j + 1 << " : ";
                        cin>>b[i][j];
                    }

                cout<<endl;
                cout<<"Macierz B: " << endl;
                for (i=0;i<o;++i)
                {
                    cout<<endl;
                    for (j=0;j<p;++j)
                        cout<<b[i][j]<<" ";
                }

                //Odejmowanie dwoch macierzy
                cout<<endl;
                cout<<endl;
                cout<<"Wynikiem odejmowania jest macierz: ";
                cout<<endl;
                for (i=0;i<m;++i)
                {
                    for (j=0;j<n;++j)
                    {
                        c[i][j]=a[i][j]-b[i][j];
                    }
                }
                for (i=0;i<m;++i)
                {
                    cout<<endl;
                    for (j=0;j<n;++j)
                        cout<<c[i][j]<<" ";
                }
            }
            cout<<endl;
            cout<<endl;
            cout<<"Aby wykonac kolejne dzialanie wcisnij Enter" << endl;
            break;

        //MNOZENIE MACIERZY
        case 3:
            cout<<"Podaj liczbe wierszy macierzy A: ";
            cin>>m;
            cout<<"Podaj liczbe kolumn macierzy A: ";
            cin>>n;
            cout<<"Podaj liczbe wierszy macierzy B: ";
            cin>>o;
            cout<<"Podaj liczbe kolumn macierzy B: ";
            cin>>p;
            if (n==o)
            {
                cout<<endl;
                cout<<"Mozna wykonac mnozenie macierzy"<<endl;
                cout<<endl;
            }
            else
            {
                cout<<endl;
                cout<<"Blad! Kolumna pierwszej macierzy nie jest rowna rzedowi drugiej.";
                cout<<endl;
                cout<<"Aby wykonac kolejne dzialanie wcisnij Enter" << endl;
                break;
                l=0;
            }

            if(l)
            {
                cout<<"Wprowadz wartosci macierzy A i potwierdzaj przyciskiem Enter: "<<endl;
                //Przechowywanie elemetnów pierwszej macierzy wprowadzonych przez u¿ytkownika
                for (i=0;i<m;++i)
                    for (j=0;j<n;++j)
                    {
                        cout << "Podaj element a" << i + 1 << j + 1 << " : ";
                        cin>>a[i][j];
                    }

                cout<<endl;
                cout<<"Macierz A: "<< endl;
                for (i=0;i<m;++i)
                {
                    cout<<endl;
                    for (j=0;j<n;++j)
                        cout<<a[i][j]<<" ";
                }
                cout<<endl;
                cout<<endl;
                cout<<"Wprowadz wartosci macierzy B i potwierdzaj przyciskiem Enter: "<<endl;
                //Przechowywanie elemetnów drugiej macierzy wprowadzonych przez u¿ytkownika
                for (i=0;i<o;++i)
                 for (j=0;j<p;++j)
                    {
                        cout << "Podaj element b" << i + 1 << j + 1 << " : ";
                        cin>>b[i][j];
                    }

                cout<<endl;
                cout<<"Macierz B: " << endl;
                for (i=0;i<o;++i)
                {
                    cout<<endl;
                    for (j=0;j<p;++j)
                        cout<<b[i][j]<<" ";
                }
                cout<<endl;
                cout<<endl;
                cout<<"Macierz wyjsciowa: ";
                for (i=0;i<m;++i) //wiersze pierwszej macierzy
                {
                    for (j=0;j<p;++j) // kolumny drugiej macierzy - daja rozmiar macierzy po mnozeniu
                    {
                        c[i][j]=0;
                        for (int k=0;k<n;++k)
                        {
                            c[i][j]=c[i][j]+(a[i][k]*b[k][j]);
                        }
                    }
                }
                for (i=0;i<m;++i)
                {
                    cout<<endl;
                    cout<<endl;
                    for (j=0;j<p;++j)
                        cout<<c[i][j]<<" ";
                        cout<<endl;
                }
            }
            cout<<endl;
            cout<<endl;
            cout<<"Aby wykonac kolejne dzialanie wcisnij Enter" << endl;
            break;

        //TRANSPOZYCJA MACIERZY
        case 4:
            cout<<"Podaj liczbe wierszy macierzy : ";
            cin>>m;
            cout<<"Podaj liczbe kolumn macierzy : ";
            cin>>n;

            if(l)
            {
                cout<<"Wprowadz wartosci macierzy i potwierdzaj przyciskiem Enter: "<<endl;
                //Przechowywanie elemetnów macierzy wprowadzonych przez u¿ytkownika
                for (i=0;i<m;++i)
                    for (j=0;j<n;++j)
                    {
                        cout << "Podaj element a" << i + 1 << j + 1 << " : ";
                        cin>>a[i][j];
                    }

                cout<<endl;
                cout<<"Macierz zostala wprowadzona: " << endl;
                for (i=0;i<m;++i)
                {
                    cout<<endl;
                    for (j=0;j<n;++j)
                        cout<<a[i][j]<<" ";
                }

                //Znajdowanie transpozycji macierzy A[][] i przechowywanie jej w tablicy trans[][].
                for(i=0; i<m; ++i)
                    for(j=0; j<n; ++j)
                    {
                       trans[j][i] = a[i][j];
                    }

                // Wyswietlanie transpozycji - wyœwietlanie tablicy trans[][].
                cout<<endl;
                cout << endl << "Transpozycja macierzy to: " << endl << endl;
                for(i = 0; i < n; ++i)
                    for(j=0; j < m; ++j)
                        {
                            cout << " " << trans[i][j];
                            if (j == m - 1)
                            cout << endl << endl;
                        }
            }
            cout<<endl;
            cout<<endl;
            cout<<"Aby wykonac kolejne dzialanie wcisnij Enter" << endl;
            break;

        //OBLICZANIE WYZNACZNIKA MACIERZY
        case 5:
            cout << "Podaj rozmiar macierzy: "; //Podajemy tylko rozmiar - wyznacznik macierzy mozna liczyc TYLKO z macierzy kwadratowej.
            cin >> m;
            cout << "Wprowadz wartosci macierzy i potwierdzaj przyciskiem Enter: "<< endl;
            for (i = 0; i < m; ++i)
                for (j = 0; j < m; ++j)
                    {
                        cout << "Podaj element a" << i + 1 << j + 1 << " : ";
                        cin >> matrix[i][j];
                    }
            cout<<endl;
            cout<<"Twoja macierz to: "<<endl;
            cout<<endl;
            for (i = 0; i < m; ++i) {
                for (j = 0; j < m; ++j)
                    cout << matrix[i][j] <<" ";
                    cout<<endl;
                }
            cout<<endl;
            cout<<"Wyznacznik macierzy wynosi:  "<< determinant(matrix, m);
            cout<<endl;
            cout<<"Aby wykonac kolejne dzialanie wcisnij Enter" << endl;
            break;


        default:
            cout<<"Nie ma takiej opcji w menu. Sprobuj jeszcze raz.";

    }
    getch(); //Powoduje ze program nie zamyka sie od razu.

}while(r!=999);
 return 0;
    }

